import numpy as np

# This objective is used for stock domain simulations.
# It expects that the price change at the time of sale is stored in posttest
# array, hence for the simple objective we are interested in - the function
#simply returns this value.
def stock_objective(observs, max_observs, posttest, test_skills, skill_id):
    assert (len(posttest) == 1)
    # Find the best change.
    bst = np.max(observs[0:max_observs])
    wst = np.min(observs[0:max_observs])
    print "bst ", bst, " wst ", wst, " posttest ", posttest[0]
    assert(bst >= posttest[0] and wst <= posttest[0])
    res = (posttest[0]-wst)/(bst-wst)  # posttest[0] -> [0,1]
    return 1.0-res  # convert to minimization


class StockPolicy:

    POLICY_SCALING = 10
    MAX_STOCK_TRAJECTORY_LEN = 55  # every 5 minutes from 9:30AM to 1:55PM
    @staticmethod
    def should_sell(traj_data, sale_time, policy_params, num_lr_extra_features):
        extracted_features = StockPolicy.extract_stock_features(
            traj_data, sale_time, num_lr_extra_features)
        if len(extracted_features) == 0: return True  # trivial policy
        threshold = policy_params[-1]
        scaled_policy_params = [e * StockPolicy.POLICY_SCALING for e in policy_params[0:-1]]
        score = 1.0/(1.0+np.exp(-np.dot(scaled_policy_params, extracted_features)))
        if ((score >= threshold or (sale_time >= StockPolicy.MAX_STOCK_TRAJECTORY_LEN-1))):
            return True
        else:
            return False

    @staticmethod
    def extract_stock_features(traj_data, sale_time, num_lr_extra_features):
        max_trajectory_len = StockPolicy.MAX_STOCK_TRAJECTORY_LEN
        assert(sale_time < max_trajectory_len)
        if (num_lr_extra_features < 0):
            return []
        elif (num_lr_extra_features == 0):
            return [traj_data[sale_time]]
        elif (num_lr_extra_features == 3):
            v = [1]  # [bias]
            v.append(traj_data[max_trajectory_len+sale_time])  # a feature
            # Use last 2 price changes.
            for t in xrange(2):
                if (sale_time-t >= 0):
                    v.append(traj_data[sale_time-t])
                else:
                    v.append(0)
            #print "sale_time ", sale_time, " v ", v
            return v
        else:
            print "Unimplemented extraction with ", num_lr_extra_features, " features"
            exit()
