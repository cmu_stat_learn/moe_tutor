#
# NOTE: not unit-tested, not regression-tested,
# contains numerous last-minute ad-hoc modifications
# (but should be still useful for experimentation).
#
import sys
import numpy as np

# hhmlean used to be included in scikitlearn, but is now developed separately
from hmmlearn import hmm
from sklearn import cross_validation
from sklearn import linear_model

from moe.optimal_learning.python.student_objectives import *

# Constants for model types supported SudentModel.
MODEL_TYPE_BKT = 'bkt'
MODEL_TYPE_LR = 'lr'
MODEL_TYPE_PTABLE = 'ptable'
MODEL_TYPE_SIMPLE = 'simple'

# Returns Logistic Regression features for the given observations.
# observ should be a list of values in {0,1}.
# num_features should be at least 1 for bias
# if num_features > 1, then #correct will be appended
# if num_features > 2, then #incorrect will be appended
# if num_features > 3, then binary feature for "contains 3 in a row" will
# be added
# if num_features > 4, then binary feature "last is correct" will be appended
# if num_features > 5, then the remaining features added will be Gaussian noise
# ~N(0,1); this is to simulate real-world situations when a number of features
# are extracted, but only a few are relevant to modeling the underlying
# phenomenon.
def extract_lr_features(observ, num_features):
    assert(num_features >= 1)  # need at least bias
    v = [1]  # [bias]
    if (num_features > 1):
        v.append(np.sum(observ))  # #corr
    if (num_features > 2):
        v.append(len(observ)-np.sum(observ))  # #incorrect
    if (num_features > 3):
        if (len(observ) >= 3):
            streak = np.convolve(observ, np.ones((3,))/3, mode='valid')
            has_streak = len(np.where(streak==1)[0]) > 0
        else:
            has_streak = False
        v.append(1 if has_streak else 0)  # was there a streak of 3 correct?
    if (num_features > 4):
        last_was_corr = ((len(observ) > 0) and observ[-1] == 1)
        v.append(1 if last_was_corr else 0)  # was the last answer correct?
    assert(num_features <= 5)
    assert(len(v) == num_features)
    return v


# Returns a score for the given vector of features using the given LR weights.
def lr_score(v, w):
    return 1.0/(1.0+np.exp(-np.dot(w, v)))


# Returns BKT params from the given HMM: [pl, pt, pg, ps].
def get_bkt_params(model):
    return [model.startprob_[1], model.transmat_[0, 1],
            model.emissionprob_[0, 1], model.emissionprob_[1, 0]]


# Initialize an HMM with the given BKT params
# (pl (initial knowledge), pt (learning) pg (guess) ps (slip)).
# Assumes that model is a two-state HMM with binary observations.
def init_hmm_with_bkt_params(model, pl, pt, pg, ps):
    model.startprob_ = np.array([1.0-pl, pl])
    model.transmat_ = np.array([[1.0-pt, pt], [0.0, 1.0]])
    model.emissionprob_ = np.array([[1.0-pg, pg], [ps, 1.0-ps]])


# Calls InitRandomHMMParams(), executes hmm.fit(observ_seqs) for each
# cross-validation fold, computes a CV score. Does this max_restarts times.
# Makes a final call to hmm.fit() with best best-performing (according to
# CV score) starting parameters.
# Uses EM to fit a two-state HMM. Technically not BKT, since we do not
# explicitly enforce the "no forgetting" during EM training.
def fit_em_with_cv_and_rand_restarts(observ_seqs, random_state,
                                     max_restarts=5, max_iter=200):
    assert(len(observ_seqs) > 0)
    # Create up to 5 folds from the training data.
    cv_observ_seqs = []
    for seq in observ_seqs:
        if len(seq) > 0: cv_observ_seqs.append(seq)
    if (len(cv_observ_seqs) == 0): return  # nothing to train from
    if (len(cv_observ_seqs) == 1):   # if only one training point - replicate it
        cv_observ_seqs = [cv_observ_seqs[0], cv_observ_seqs[0]]  # for a 2-fold cv
    cv = cross_validation.KFold(n=len(cv_observ_seqs),
                                n_folds=min(len(cv_observ_seqs), 5))
    # Do several random restarts to find the best local optimum.
    bst_cv_score = -float("inf")
    [bst_strt_pl, bst_strt_pt, bst_strt_pg, bst_strt_ps] = [0, 0, 0, 0]
    for rstrt in range(max_restarts):
        # Meaningful ranges are also enforced for BO in config*.json
        [rnd_pl, rnd_pg, rnd_ps] = random_state.randint(1, 40, 3)/100.0
        [rnd_pt] = random_state.randint(1, 60, 1)/100.0
        # print "rstrt", rstrt, "rnd params:", rnd_pl, rnd_pt, rnd_pg, rnd_ps
        cv_score = 0.0  # will hold the sum of log likelihoods of test folds
        for train_indices, test_indices in cv:
            # print('Train: %s | test: %s' % (train_indices, test_indices))
            model = hmm.MultinomialHMM(n_components=2, n_iter=100,
                                       random_state=random_state,
                                       init_params='')  # empty for restarts
            init_hmm_with_bkt_params(model, rnd_pl, rnd_pt, rnd_pg, rnd_ps)
            train_observ_seqs = [cv_observ_seqs[i] for i in train_indices]
            # Fit only if we have more data than a single observation.
            if (len(train_observ_seqs) > 1 or train_observ_seqs[0].shape[0] > 1):
                model.fit(train_observ_seqs)
            # Test the model on the hold-out set.
            for test_idx in test_indices:
                cv_score += model.score(cv_observ_seqs[test_idx])
        # print " cv_score result: ", cv_score
        if (cv_score > bst_cv_score):
            bst_cv_score = cv_score
            [bst_strt_pl, bst_strt_pt, bst_strt_pg, bst_strt_ps] = \
                [rnd_pl, rnd_pt, rnd_pg, rnd_ps]
    # Re-train with the best initial params.
    # print "bst_cv_score ", bst_cv_score
    # print "bst_strt_* ", bst_strt_pl, bst_strt_pt, bst_strt_pg, bst_strt_ps
    model = hmm.MultinomialHMM(n_components=2, n_iter=max_iter,
                               random_state=random_state,
                               init_params='')  # empty for random restarts
    init_hmm_with_bkt_params(model, bst_strt_pl, bst_strt_pt,
                             bst_strt_pg, bst_strt_ps)
    if (len(train_observ_seqs) > 1 or train_observ_seqs[0].shape[0] > 1):
        model.fit(cv_observ_seqs)  # now train on all the data
    print "final params ", get_bkt_params(model)
    return model


# Evaluates the given threshold under the given model.
def score_model_approx(true_model, learned_model,
                       max_problems, objective,
                       num_iter, random_state, num_posttest_answers=1, debug=False):
    objective_values = []
    #print "score_model_approx LR ", model.get_params(), " thresh ", model.get_thresh()
    for itr in xrange(num_iter):
        [all_observ, true_states] = true_model.sample(max_problems, random_state)
        #print "all_observ ", all_observ, " true_states ", true_states
        observs = []
        for ob in xrange(len(all_observ)+1):
            if learned_model.should_stop(observs, max_problems):
                post_corr = true_model.post_corr(observs, true_states, random_state)
                posttest_answers = random_state.binomial(1, post_corr,
                                                         size=num_posttest_answers)
                posttest_skills = np.zeros(num_posttest_answers)
                objective_values.append(globals()[objective](
                    observs, max_problems, posttest_answers, posttest_skills, 0))
                if debug:
                    print "all_observ ", all_observ, " observs ", observs, \
                        " posttest_answers ", posttest_answers, " obj ", objective_values[-1]
                break
            if (ob < len(all_observ)): observs.append(all_observ[ob])
        #print "observs ", observs, " -> ", objective_values[-1]
    return np.sum(objective_values)/len(objective_values)


def adjust_means_tutor(points_to_sample, mus, mean_fxn_args):
    """Shift means in mus using the given mean function adjustment arguments"""
    random_state = np.random.RandomState()
    # We are going to work with deterministic policies for now.
    deterministic_policy = True
    mboa_fit_model = StudentModel(mean_fxn_args['mboa_model_type'],
                                  mean_fxn_args['num_lr_extra_params'],
                                  random_state, deterministic_policy)
    mboa_fit_model.init_with_params(mean_fxn_args['mboa_fit_params'])
    mboa_fit_model.set_thresh(mean_fxn_args['mboa_fit_thresh'])
    sampled_model = StudentModel(mean_fxn_args['bo_model_type'],
                                 mean_fxn_args['num_lr_extra_params'],
                                 random_state, deterministic_policy)
    mu_star_adjusted = np.zeros(len(mus))
    assert(len(points_to_sample) == len(mus))
    for idx in xrange(len(mus)):
        sampled_model.init_with_params(points_to_sample[idx][0:-1])
        sampled_model.set_thresh(points_to_sample[idx][-1])
        # We used to take just a single sample, and this was very very noisy.
        adjustment = mean_fxn_args['beta'] * score_model_approx(
            mboa_fit_model, sampled_model, mean_fxn_args['max_problems'],
            mean_fxn_args['objective'], 5, random_state, 1, False)
        mu_star_adjusted[idx] = mus[idx] + adjustment
    return mu_star_adjusted

#
# A class for student modeling. Includes capability to initialize student models
# with existing set of parameters and get P(know) for models like BLT, LR.
# Provides scikit-learn-based functionality to learn HMM parameters with EM and
# LR parameters with regression.
#
class StudentModel:
    """Class for representing learned model"""
    def __init__(self, model_type, num_lr_extra_params, random_state,
                 deterministic_policy=True):
        self.__deterministic_policy = deterministic_policy
        self.__random_state = random_state
        self.__thresh = random_state.random_sample()  # random thresh in [0,1)
        """Initializes self according to the given model_type"""
        self.__model_type = model_type
        if self.__model_type == MODEL_TYPE_BKT:
            self.__model = hmm.MultinomialHMM(n_components=2,
                                              random_state=random_state)
            # Meaningful ranges should are also enforced for BO in config*.json
            # and for MOE in domain
            [rnd_pl, rnd_pg, rnd_ps] = random_state.randint(1, 40, 3)/100.0
            [rnd_pt] = random_state.randint(1, 60, 1)/100.0
            #print "rnd_pl, rnd_pt, rnd_pg, rnd_ps ", rnd_pl, rnd_pt, rnd_pg, rnd_ps
            init_hmm_with_bkt_params(self.__model, rnd_pl, rnd_pt, rnd_pg, rnd_ps)
        elif self.__model_type == MODEL_TYPE_LR:
            # Meaningful ranges are also enforced for BO in config*.json
            [rnd_b] = random_state.randint(-100, 100, 1)/100.0
            params = [rnd_b]
            if (num_lr_extra_params > 0):
                params.extend(random_state.randint(-100, 100,
                                                   num_lr_extra_params)/100.0)
            self.__model = params
        elif self.__model_type == MODEL_TYPE_PTABLE:
            self.__model = [0.5]  # uninformed PTABLE
        elif self.__model_type == MODEL_TYPE_SIMPLE:
            self.__model = [0]  # 0 questions to "learned"
        else:
            print "Model type ", self.__model_type, " not supported"
            assert(False)

    def init_with_params(self, params):
        if self.__model_type == MODEL_TYPE_BKT:
            init_hmm_with_bkt_params(self.__model, *params)
        else:  # LR, PTABLE, SIMPLE
            self.__model = params[:]  # make a copy

    def init_with_bkt_params(self, pl, pt, pg, ps):
        """Initializes self.model with given BKT params (if __isBKT==True)."""
        assert(self.__model_type == MODEL_TYPE_BKT)
        init_hmm_with_bkt_params(self.__model, pl, pt, pg, ps)

    def init_with_lr_params(self, lr_params):
        """Initializes self.model with given LR params (if __isBKT==False)."""
        assert(self.__model_type == MODEL_TYPE_LR)
        self.__model = lr_params[:]  # make a copy

    def is_bkt(self): return (self.__model_type == MODEL_TYPE_BKT)

    def get_model_type(self): return self.__model_type

    def num_lr_extra_params(self):
        if self.__model_type == MODEL_TYPE_LR:
            return len(self.__model)-1
        else:
            return 0

    def num_params(self):
        if self.__model_type == MODEL_TYPE_BKT:
            return 4
        else:
            return len(self.__model)

    def get_params(self):
        """Returns the list of parameters of this model:
        [pl pt pg ps] if __isBKT==True (self.model is HMM for BKT), otherwise
        [beta_intercept beta_correct ... ] (self.model is LR)."""
        if self.__model_type == MODEL_TYPE_BKT:
            return get_bkt_params(self.__model)
        else:  # is LR, PTABLE, SIMPLE
            return self.__model[:]  # make a copy

    def get_thresh(self): return self.__thresh

    def set_thresh(self, thresh): self.__thresh = thresh

    def sample(self, max_problems, random_state):
        """Returns [observations true_states] each of length max_problems
        sampled from previously initialized self.__model"""
        if self.__model_type == MODEL_TYPE_BKT:
            return self.__model.sample(n=max_problems, random_state=random_state)
        elif self.__model_type == MODEL_TYPE_LR:
            observ = []
            true_states = []
            pcorr = lr_score(extract_lr_features(  # initial prob corr
                observ, len(self.__model)), self.__model)
            for _ in xrange(max_problems):
                # note: calling append before extracting features, since
                # true_states[0] indicates the state of the student after
                # answering the first question (not before).
                observ.append(random_state.binomial(1, pcorr))
                v = extract_lr_features(observ, len(self.__model))
                pcorr = lr_score(v, self.__model)
                # TODO(rika): ok to define "true" state using LR this way?
                true_states.append(1 if pcorr >= self.__thresh else 0)
            assert(len(observ) == max_problems)
            return [observ, true_states]
        elif self.__model_type == MODEL_TYPE_PTABLE:
            #print "sample using PTABLE ", self.__model
            observ = []
            for i in xrange(max_problems):
                observ.append(random_state.binomial(1, self.__model[0]))
                #if i == 0:
                #    observ.append(random_state.binomial(1, self.__model[0]))
                #else:
                #    pr = self.__model[1] if observ[-1] == 0 else self.__model[2]
                #    observ.append(random_state.binomial(1, pr))
            #print "observ", observ
            return [observ, observ]  # no good notion of true states here
        elif self.__model_type == MODEL_TYPE_SIMPLE:
            num = int(round(random_state.normal(self.__model[0], 1.0, 1)))
            observ = [0]*num
            observ.extend([1]*(max_problems-num))
            return [observ, observ]  # no good notion of true states here
        else:
            print "Model type ", self.__model_type, " not supported"
            assert(False)

    def post_corr(self, observs, true_states, random_state):
        """Returns posterior probability of answering questions correctly
        from now on (that is after "learning" from all the previous questions,
        including the last one. ) and true_states (true_states only used for
        BKT models) """
        if self.__model_type == MODEL_TYPE_BKT:
            if len(observs) == 0: return self.__model.startprob_[1]  # pl
            num_problems_solved = len(observs)
            last_actual_state_id = num_problems_solved-1
            return self.__model.emissionprob_[true_states[last_actual_state_id], 1]
        elif self.__model_type == MODEL_TYPE_LR:
            return self.plearned(observs)
        elif self.__model_type == MODEL_TYPE_PTABLE:
            return self.__model[0]
            #if len(observs) == 0:
            #    return self.__model[0]
            #else:
            #    return self.__model[1] if observs[-1] == 0 else self.__model[2]
        elif self.__model_type == MODEL_TYPE_SIMPLE:
            if (len(observs) < 10):
                return 0.0  # did not learn
            else:
                return 1.0 if (sum(observs[-10:]) == 10) else 0.0
        else:
            print "Model type ", self.__model_type, " not supported"
            assert(False)

    def plearned(self, observs):
        """Returns P(learned) for BKT models,
        P(correct on next) for LR models"""
        if self.__model_type == MODEL_TYPE_BKT:
            if len(observs) == 0:
                return self.__model.startprob_[1]  # pl
            else:
                res = self.__model.predict_proba(observs)
                return res[res.shape[0]-1, 1]
        elif self.__model_type == MODEL_TYPE_LR:
            v = extract_lr_features(observs, len(self.__model))
            return lr_score(v, self.__model)
        else:
            print "Model type ", self.__model_type, " not supported"
            assert(False)

    def should_stop(self, observs, max_problems):
        """Returns True if should stop giving further questions."""
        if (len(observs) == max_problems): return True  # ran out of problems
        if (self.__deterministic_policy):
            return (self.plearned(observs) >= self.__thresh)
        else:
            rnd_num = self.__random_state.uniform(0, 1, 1)
            return (rnd_num <= self.plearned(observs))


    def learn_thresh(self, max_problems, objective, num_postest_answers,
                     random_state, max_num_iter=50):
        assert(self.__model_type != MODEL_TYPE_PTABLE)
        assert(self.__model_type != MODEL_TYPE_SIMPLE)
        # After fitting the model, find the best-performing threshold for the
        # for the given objective for this model.
        THRESH_NDISCR = 20
        best_th_score = None
        best_th = None
        for th in np.linspace(0.0, 1.0, num=THRESH_NDISCR):
            self.__thresh = th
            th_score = score_model_approx(
                self, self, max_problems, objective, max_num_iter,
                random_state, num_postest_answers)
            if ((best_th_score is None) or (th_score < best_th_score)):
                best_th_score = th_score
                best_th = th
        self.__thresh = best_th

    def learn(self, observ_seqs, max_problems, objective,
              num_posttest_answers, random_state):
        """Do model fitting using the given list of observation sequences"""
        if self.__model_type == MODEL_TYPE_BKT:
            self.__model = fit_em_with_cv_and_rand_restarts(observ_seqs,
                                                            random_state)
            self.learn_thresh(max_problems, objective,
                              num_posttest_answers, random_state)
        elif self.__model_type == MODEL_TYPE_LR:
            # Emma suggested constructing logistic regression input data as
            # follows: suppose have training data from two students;
            # suppose the first student answered all the 3 problems correctly,
            # and the second only answered the last problem correctly.
            # Then the dataset would look like this:
            # [1 0] -> 1
            # [2 0] -> 1
            # [3 0] -> 1
            # [0 1] -> 0
            # [0 2] -> 0
            # [1 2] -> 1
            # Implementation note: conversion from observ_seqs to X could be
            # made more efficient if needed (currently is not the bottleneck,
            # so the brevity of the code is favored in this case).
            X = []
            y = []
            for seq in observ_seqs:
                for t in xrange(len(seq)):
                    v = extract_lr_features(seq[0:t], len(self.__model))
                    X.append(v[1:])  # fit() does not need bias
                    y.append(seq[t])
            # Determine number of cross-validation folds, then run fit().
            numones = np.count_nonzero(y)
            nfolds = np.min([numones, len(y)-numones, 5])
            if nfolds < 1:
                self.__init__(self.__model_type, self.num_lr_extra_params(),
                              random_state, self.__deterministic_policy)
                return  # can't fit, so try a different set of random params
            if nfolds == 1:
                logreg = linear_model.LogisticRegression(
                    C=1e5, random_state=random_state)
            else:
                logreg = linear_model.LogisticRegressionCV(cv=nfolds)
            logreg.fit(X, y)
            new_params = [logreg.intercept_[0]]
            new_params.extend(logreg.coef_[0])
            self.__model = new_params
            self.learn_thresh(max_problems, objective,
                              num_posttest_answers, random_state)
        elif self.__model_type == MODEL_TYPE_PTABLE:
            print "Compute PTABLE"
            # Note: at first we wanted to try a simple probability table approach,
            # but found that we needed an even more restricted model.
            # So this model currently uses simply a probability of seeing a
            # correct answer (essentially, just a bias, no features).
            # This is just to test a corner case of what happens when one is
            # working with very restricted models.
            #tbl = np.ones(4)  # Laplace smoothing
            #prior_ones = 0
            ones = 0
            total = 0
            for seq in observ_seqs:
                ones += np.sum(seq)
                total += len(seq)
            #    if len(seq > 0): prior_ones += seq[0]
            #    print "seq ", seq
            #    for t in xrange(len(seq)-1):
            #        tbl[seq[t]*2+seq[t+1]] += 1
            #self.__model[0] = 1.0*prior_ones/len(observ_seqs)
            #self.__model[1] = 1.0*tbl[1]/(tbl[0]+tbl[1])
            #self.__model[2] = 1.0*tbl[3]/(tbl[2]+tbl[3])
            self.__model[0] = 1.0*ones/total
            self.__thresh = None
            print "PTABLE ", self.__model
        elif self.__model_type == MODEL_TYPE_SIMPLE:
            print "Compute SIMPLE"
            nums = [max_problems]
            for seqid in xrange(len(observ_seqs)):
                for answid in xrange(len(observ_seqs[seqid])):
                    if observ_seqs[seqid][answid] == 1:
                        nums.append(answid)
                        break
                print observ_seqs[seqid], "->", nums[-1]
            print "mean ", np.mean(nums)
            self.__model = [np.mean(nums)]
            sys.stdout.flush()
        else:
            print "Model type ", self.__model_type, " not supported"
            assert(False)
