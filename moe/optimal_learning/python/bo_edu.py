#
# NOTE: This code was modified hastily in a rush for
# conference deadline. Most functions are not unit-tested.
#
#
import cPickle
import numpy as np
import scipy
import sys
import time

from moe.easy_interface.experiment import Experiment
from moe.easy_interface.simple_endpoint import gp_next_points
from moe.optimal_learning.python.cpp_wrappers.gaussian_process import GaussianProcess
from moe.optimal_learning.python.cpp_wrappers.covariance import SquareExponential
from moe.optimal_learning.python.student_model import StudentModel as StudentModel
from moe.optimal_learning.python.student_model import MODEL_TYPE_BKT
from moe.optimal_learning.python.student_model import MODEL_TYPE_LR

from moe.optimal_learning.python.student_objectives import *

from moe.optimal_learning.python.stock_policy import StockPolicy
from moe.optimal_learning.python.stock_policy import stock_objective

# BKT parameter bounds: pt<0.4, pl<0.6, pg<0.4, ps<0.4
MOE_BKT_DOMAIN_BOUNDS = [[0.01, 0.4], [0.01, 0.6], [0.01, 0.4], [0.01, 0.4]]
MOE_NOISE_VARIANCE = 0.05   # manual computation gave 0.05 (0.25 is max for bin)
MOE_EXTRA_TRAIN_NOISE_VARIANCE = 0.1  # for moe_get_proxy_score_short()
# Tuned static hyper parameters for BO for LR with MOE
# TODO(rika): optimize these for BKT, not LR for real tutor experiments.
MOE_PRIOR_VARIANCE = 0.3  # gp_hyper_opt() gave 0.1924-0.3897
MOE_LENGTH_SCALE = 2.0  # gp_hyper_opt() gave 2.0
MOE_LENGTH_SCALE_THRESH = 0.2  # gp_hyper_opt() gave 0.5-0.24

# TODO(rika): the constants below should be moved to turk part of the code.
DEFAULT_MOE_PORT = 6543
DEFAULT_NUM_SKILLS = 8  # Number of different skills for STAT101 problems
#DEFAULT_TEST_SKILLS = [0, 1, 2, 3, 2, 1, 4, 5, 4, 3, 6, 7, 5]
# ATTENTION: skill_id 5 ("x axis") had 13 instead of 16 problems during BO experiment.
# This error must have been introduced when Rika and Dexter were typing in
# the maximum number of problems (Dexter was dictating, Rika typing).
# We did check the numbers twice, but the error still got in somehow.
#DEFAULT_MAX_PROBLEMS_PER_SKILL = [4, 4, 3, 3, 16, 4, 17, 2]
# Maximum number of problems for second BO live experiment.
# See data/problist.tsv
DEFAULT_MAX_PROBLEMS_PER_SKILL = [4, 5, 3, 4, 16, 4, 16, 2]
SKILL_ID_TO_SKILL_NAME = {0: 'd to h', 1: 'y axis', 2: 'h to d', 3: 'center',
                          4: 'shape', 5: 'x axis', 6: 'histogram', 7: 'spread'}
SKILL_NAME_TO_SKILL_ID = {v: k for k, v in SKILL_ID_TO_SKILL_NAME.items()}

class BOEDUExperiment:
    def __init__(self, num_skills, model_type, num_lr_extra_params, moe_port,
                 deterministic_policy):
        self.__model_type = model_type
        self.__num_lr_extra_params = num_lr_extra_params
        self.__deterministic_policy = deterministic_policy
        self.__moe_port = moe_port
        if model_type == MODEL_TYPE_BKT:
            self.__moe_domain_bounds = MOE_BKT_DOMAIN_BOUNDS[:]
        else:
            self.__moe_domain_bounds = [[-1, 1]] * (num_lr_extra_params+1)
        self.__moe_domain_bounds.append([0, 1])
        hyper_params = [MOE_PRIOR_VARIANCE]
        hyper_params.extend([MOE_LENGTH_SCALE]*self.get_num_params())
        hyper_params.append(MOE_LENGTH_SCALE_THRESH)
        self.__moe_covariance_info = {'covariance_type': 'square_exponential',
                                      'hyperparameters': hyper_params}
        self.__moe_exps = []
        for skill_id in xrange(num_skills):
            self.__moe_exps.append(Experiment(self.__moe_domain_bounds))

    def get_model_type(self): return self.__model_type
    def get_num_lr_extra_params(self): return self.__num_lr_extra_params
    def get_deterministic_policy(self): return self.__deterministic_policy
    def get_moe_port(self): return self.__moe_port
    def get_num_params(self): return len(self.__moe_domain_bounds)-1
    def get_moe_exp(self, skill_id): return self.__moe_exps[skill_id]
    def get_moe_covariance_info(self): return self.__moe_covariance_info
    def get_moe_domain_bounds(self): return self.__moe_domain_bounds
    def num_skills(self): return len(self.__moe_exps)


class Trajectory:
    def __init__(self, num_skills, pretest, posttest, test_skills,
                 observs, observs_skills):
        self.num_skills = num_skills
        self.pretest = np.array(pretest)  # list of 0/1 answers for pretest
        self.posttest = np.array(posttest)  # list of 0/1 answers for posttest
        # List of skill IDs for each of test questions (should have the same
        # length as pre/posttest).
        self.test_skills = np.array(test_skills)
        # Answers to problems given by the tutor.
        self.observs = np.array(observs)
         # List of skill IDs for each question in observs.
        self.observs_skills = np.array(observs_skills)
        # Compute useful auxiliary data.
        self.init_aux()

    def init_aux(self):
        # For convenience record the data for all the skills separately too.
        self.observs_per_skill = {}
        self.pretest_per_skill = {}
        self.posttest_per_skill = {}
        for skill_id in xrange(self.num_skills):
            self.observs_per_skill[skill_id] = self.observs[np.where(
                self.observs_skills==skill_id)[0]]
            self.pretest_per_skill[skill_id] = self.pretest[np.where(
                self.test_skills==skill_id)[0]]
            self.posttest_per_skill[skill_id] = self.posttest[np.where(
                self.test_skills==skill_id)[0]]

    def printShort(self):
        print self.pretest, self.posttest, \
            self.test_skills, self.observs, self.observs_skills

    def printDetailed(self):
        print "Trajectory:"
        print "pretest", self.pretest, " posttest ", self.posttest
        print "test_skills ", self.test_skills
        print "observs ", self.observs
        print "observs_skills ", self.observs_skills
        for skill_id in xrange(self.num_skills):
            print "skill_id ", skill_id, ": ", self.observs_per_skill[skill_id], \
                self.posttest_per_skill[skill_id]

    def posttest_proxy(self, num_skills, skill_id, num_problems_seen_on_skill):
        """Returns a posttest proxy constructed from the given trajectory
        assuming that the student saw only the given number of problems on
        the given skill. We take one problem for each skill the part of the
        trajectory after num_problems_seen_on_skill were given to the student.
        For the skills that contain no problems in the remaining part of
        observs, answers on posttest problems are used instead.
        Example for two skills (skill_ids 3 and 4):
        observs [0 0 1 0] observs_skills [3 4 3 4]
        posttest [1 1] posttest_skills [3 4]
        skil_id=3 num_problems_seen_on_skill=2
        posttest_proxy [1 0] posttest_skills[3 4]
        """
        proxy_answers_start_id = 0
        if num_problems_seen_on_skill > 0:
            occur_id = num_problems_seen_on_skill-1
            idxs = np.where(self.observs_skills==skill_id)[0]
            assert(num_problems_seen_on_skill <= len(idxs))
            stopping_id = idxs[occur_id]
            proxy_answers_start_id = stopping_id+1
        # Note: if proxy_answers_start_id exceeds length of observs we will
        # simply get an empty array of proxy_answers
        proxy_answers = self.observs[proxy_answers_start_id:]
        proxy_answers_skills = self.observs_skills[proxy_answers_start_id:]
        posttest_proxy = []
        posttest_proxy_skills = []
        for tmp_skill_id in xrange(num_skills):
            if len(proxy_answers) == 0:
                proxy_answers_this_skill = []
            else:
                proxy_answers_this_skill = proxy_answers[np.where(proxy_answers_skills==tmp_skill_id)]
            if (len(proxy_answers_this_skill) > 0):
                posttest_proxy.append(proxy_answers_this_skill[0])
                posttest_proxy_skills.append(tmp_skill_id)
            else:
                posttest_this_skill = self.posttest[np.where(self.test_skills==tmp_skill_id)]
                posttest_proxy.extend(posttest_this_skill)
                posttest_proxy_skills.extend([tmp_skill_id]*len(posttest_this_skill))
        return posttest_proxy, posttest_proxy_skills


# Returns the best-looking point in the given Gaussian Process.
def moe_compute_best_pt_info(moe_exp, covariance_info, confidence=None,
                             mean_fxn_info=None, sample_pts=None, debug=False):
    if moe_exp.historical_data.num_sampled <= 0: return [None, None, None]
    covar = SquareExponential(covariance_info['hyperparameters'])
    cpp_gp = GaussianProcess(covar, moe_exp.historical_data,
                             mean_fxn_info=mean_fxn_info)
    if (sample_pts == None):
        sample_pts = np.array(moe_exp.historical_data.points_sampled)
    #moe_pts_r = np.array(moe_exp.historical_data.points_sampled)
    #moe_pts_d = moe_exp.domain.generate_uniform_random_points_in_domain(100)
    #sample_pts = np.concatenate((moe_pts_r, moe_pts_d), axis=0)
    cpp_mu = cpp_gp.compute_mean_of_points(sample_pts, debug)
    cpp_var = np.diag(cpp_gp.compute_variance_of_points(sample_pts))
    if debug: print "sample_pts ", sample_pts, "\ncpp_mu ", cpp_mu, "\ncpp_var ", cpp_var
    if confidence is None:
        minidx = np.argmin(cpp_mu)
    else:
        upper_conf = scipy.stats.norm.interval(confidence, loc=cpp_mu,
                                               scale=np.sqrt(cpp_var))[1]
        minidx = np.argmin(upper_conf)
        if debug: print " cpp_var ", cpp_var[minidx], " upper_conf ", upper_conf[minidx]
    if debug: print "cpp_mu ", cpp_mu[minidx], " best_moe_pt ", sample_pts[minidx]
    return [sample_pts[minidx], cpp_mu[minidx], cpp_var[minidx]]


# Helper function to identify the best point using the GP.
def moe_compute_best_pt(moe_exp, covariance_info, confidence=None,
                        mean_fxn_info=None, debug=False):
    return moe_compute_best_pt_info(moe_exp, covariance_info, confidence,
                                    mean_fxn_info=mean_fxn_info, debug=debug)[0]

# Helper function to identify the mean of the best point using the GP.
def moe_compute_best_mean(moe_exp, covariance_info, confidence=None,
                        mean_fxn_info=None, debug=False):
    return moe_compute_best_pt_info(moe_exp, covariance_info, confidence,
                                    mean_fxn_info=mean_fxn_info, debug=debug)[1]


# Probability of knowing the skill computed using only pretest score evidence.
def prob_prior_knowledge(pretest, test_skills, skill_id, training_model):
    assert(isinstance(pretest, (np.ndarray, np.generic)))
    assert(isinstance(test_skills, (np.ndarray, np.generic)))
    if (len(pretest) == 0 or len(np.where(test_skills==skill_id)[0]) == 0): return 0.0
    pretest_this_skill = pretest[np.where(test_skills==skill_id)[0]]
    # This is what Dexter does in the server side of the code.
    # Violates no-learning assumption for pretest problems with no feedback.
    # But we strongly suspect this assumption is wrong anyways.
    print "pretest_this_skill ", pretest_this_skill
    return training_model.plearned(pretest_this_skill)
    #if len(pretest_this_skill) == 0: return 0.0
    #return 1.0*sum(pretest_this_skill)/len(pretest_this_skill)


# Returns a proxy score and var for using the given model (none if no sequences
# in observ_seqs could be used for the given model).
def moe_get_proxy_score(trajectories, num_skills, skill_id, max_problems,
                        objective, training_model, seqnum_vec, random_state):
    for seqnumid in xrange(len(seqnum_vec)):
        seqnum = seqnum_vec[seqnumid]
        stop_found = False
        observs_this_skill = trajectories[seqnum].observs_per_skill[skill_id]
        observs_again_this_skill = []
        prior_know = prob_prior_knowledge(
            trajectories[seqnum].pretest, trajectories[seqnum].test_skills,
            skill_id, training_model)
        print "seq ", seqnumid, " prior_know ", prior_know,\
            "training_model ", training_model.get_params(), \
            training_model.get_thresh()
        if (prior_know > training_model.get_thresh()) or (training_model.get_thresh() <= 0):
            stop_found = True  # not giving any problems for this skill
        else:
            for ob in observs_this_skill:
                observs_again_this_skill.append(ob)
                if training_model.should_stop(observs_again_this_skill, max_problems):
                    stop_found = True
                    break
        if not stop_found:
            print len(observs_this_skill), " observs, max ", max_problems, \
                " not used for proxy"
            continue  # this sequence is not useful
        [posttest_proxy, posttest_proxy_skills] = \
            trajectories[seqnum].posttest_proxy(
                num_skills, skill_id, len(observs_again_this_skill))
        val = globals()[objective](
            observs_again_this_skill, max_problems,
            posttest_proxy, posttest_proxy_skills, skill_id)
        print "skill ", skill_id, " val ", val, \
            " observs_again_this_skill ", observs_again_this_skill, \
            " posttest_proxy ", posttest_proxy, \
            " posttest_proxy_skill ", posttest_proxy_skills
        return val, MOE_EXTRA_TRAIN_NOISE_VARIANCE, np.delete(seqnum_vec, seqnumid)
    return None, None, seqnum_vec


# Returns a proxy score for the stock domain
def moe_get_stock_proxy_score(trajectories, policy_params, objective, seqnum_vec,
                              num_lr_extra_features):
    skill_id = 0
    for seqnumid in xrange(len(seqnum_vec)):
        seqnum = seqnum_vec[seqnumid]
        observs = trajectories[seqnum].observs_per_skill[skill_id]
        sale_time = -1
        for t in xrange(StockPolicy.MAX_STOCK_TRAJECTORY_LEN):
            if StockPolicy.should_sell(observs, t, policy_params, num_lr_extra_features):
                sale_time = t
                break
        assert(sale_time >= 0 and sale_time < StockPolicy.MAX_STOCK_TRAJECTORY_LEN)
        posttest_proxy = [observs[sale_time]]
        val = globals()[objective](observs, StockPolicy.MAX_STOCK_TRAJECTORY_LEN,
                                   posttest_proxy, [0], skill_id)
        print " observs ", observs, " posttest_proxy ", posttest_proxy, " sale_time ", sale_time
        return val, MOE_EXTRA_TRAIN_NOISE_VARIANCE, np.delete(seqnum_vec, seqnumid)
    return None, None, seqnum_vec  # for now not reachable


# Populates BO GP (in moe_exp) with very noisy though useful preliminary
# estimates of the performance on the objective function.
# Returns next interesting MOE point.
# Only re-use each trajectory num_resample times.
# Re-start BO training from scratch after each real-world step.
def moe_extra_training(trajectories, skill_id, max_problems,
                       objective, edu_exp, num_resample, random_state,
                       stockdomain=False, gp_output_base=None,
                       random_trajectories=None):
    print "--------------- moe_extra_training with ", len(trajectories), " trajectories"
    if random_trajectories is not None:
        "using", len(random_trajectories), "random_trajectories"
    gp_output_file = None
    if gp_output_base is not None:
        gp_output_file = gp_output_base + "_skill" + str(skill_id) + "_gp.p"
    moe_exp = edu_exp.get_moe_exp(skill_id)
    covar_info = edu_exp.get_moe_covariance_info()
    moe_port = edu_exp.get_moe_port()
    # Override EI's best_so_far point if a conservative BO points
    # are needed for real-world trajectories.
    override_best_so_far = moe_compute_best_mean(moe_exp, covar_info)  # mean
    if (len(trajectories) == 0):
        first_moe_pt = gp_next_points(
            moe_exp, num_to_sample=1, covariance_info=covar_info,
            rest_port=moe_port, override_best_so_far=override_best_so_far)[0]
        return first_moe_pt, moe_compute_best_pt_info(moe_exp, covar_info, confidence=0.68)
        #return first_moe_pt, moe_compute_best_pt_info(moe_exp, covar_info)
    # Reset moe_exp and start populating BO model from scratch.
    # Not using this approach, since prohibitively expensive computationally.
    resmpl_moe_exp = Experiment(
        edu_exp.get_moe_domain_bounds(),
        points_sampled=moe_exp.historical_data.to_list_of_sample_points())
    if not stockdomain:
        training_model = StudentModel(edu_exp.get_model_type(),
                                      edu_exp.get_num_lr_extra_params(),
                                      random_state,
                                      edu_exp.get_deterministic_policy())
    if (random_trajectories is None):
        resample_trajectories = trajectories
    else:
        resample_trajectories = random_trajectories
    seqnum_vec = random_state.permutation(range(len(resample_trajectories))*num_resample)
    print "--------------- seqnum_vec ", seqnum_vec

    while seqnum_vec.shape[0] > 0:
        next_moe_pt = gp_next_points(resmpl_moe_exp, num_to_sample=1,
                                     covariance_info=covar_info, rest_port=moe_port)[0]
        print "moe_extra_training next_moe_pt ", next_moe_pt
        if not stockdomain:
            training_model.init_with_params(next_moe_pt[0:-1])
            training_model.set_thresh(next_moe_pt[-1])
            proxy_score, proxy_var, seqnum_vec = moe_get_proxy_score(
                resample_trajectories, edu_exp.num_skills(), skill_id, max_problems,
                objective, training_model, seqnum_vec, random_state)
        else:  # stockdomain
            proxy_score, proxy_var, seqnum_vec = moe_get_stock_proxy_score(
                resample_trajectories, next_moe_pt, objective, seqnum_vec,
                edu_exp.get_num_lr_extra_params())
        if proxy_score is None:  # can't use existing data
            if gp_output_file is not None: cPickle.dump(  # temp dump for softmax policies
                resmpl_moe_exp.historical_data.to_list_of_sample_points(),
                open(gp_output_file, 'wb'))
            return next_moe_pt, moe_compute_best_pt_info(
                resmpl_moe_exp, covar_info, confidence=0.68)
        resmpl_moe_exp.historical_data.append_sample_points(
            [[next_moe_pt, proxy_score, proxy_var]])
        print "MOE recorded proxy ", next_moe_pt, "->", proxy_score, "(", proxy_var, ")"
        sys.stdout.flush()
    next_moe_pt = gp_next_points(
        resmpl_moe_exp, num_to_sample=1, covariance_info=covar_info,
        rest_port=moe_port, override_best_so_far=override_best_so_far)[0]
    if gp_output_file is not None: cPickle.dump(  # temp dump for softmax policies
        resmpl_moe_exp.historical_data.to_list_of_sample_points(),
        open(gp_output_file, 'wb'))
    return next_moe_pt, moe_compute_best_pt_info(resmpl_moe_exp, covar_info, confidence=0.68)
    #return next_moe_pt, moe_compute_best_pt_info(resmpl_moe_exp, covar_info)

# Returns next MOE point of interest after using each student trajectory
# num_resample times for re-sampling.
# This function should be used for real-world experiments.
# If traj_info is set, quickstart kernel is used.
def next_moe_pts_edu(edu_exp, trajectories, objective, random_state,
                     max_problems_per_skill=None, num_resample=1,
                     traj_info=None, stockdomain=False, gp_output_file=None,
                     random_trajectories=None):
    print "next_moe_pts_edu", "random_trajectories", random_trajectories
    start_tm = time.time()
    moe_next_pts = []
    moe_best_pts = []
    moe_means = []
    moe_vars = []
    if max_problems_per_skill is None:
        max_problems_per_skill_vec = DEFAULT_MAX_PROBLEMS_PER_SKILL
    else:
        max_problems_per_skill_vec = max_problems_per_skill
    for skill_id in xrange(edu_exp.num_skills()):
        if (num_resample > 0):
            assert(traj_info == None)
            next_moe_pt, best_moe_info = moe_extra_training(
                trajectories, skill_id, max_problems_per_skill_vec[skill_id],
                objective, edu_exp, num_resample, random_state,
                stockdomain, gp_output_file, random_trajectories)
        else:
            next_moe_pt = gp_next_points(
                edu_exp.get_moe_exp(skill_id), num_to_sample=1,
                covariance_info=edu_exp.get_moe_covariance_info(),
                rest_port=edu_exp.get_moe_port(), traj_info=traj_info)[0]
            best_moe_info = moe_compute_best_pt_info(
                edu_exp.get_moe_exp(skill_id), edu_exp.get_moe_covariance_info())
        moe_next_pts.append(next_moe_pt)
        moe_best_pts.append(best_moe_info[0])
        moe_means.append(best_moe_info[1])
        moe_vars.append(best_moe_info[2])
        print "skill_id ", skill_id, "next next_moe_pt ", next_moe_pt, \
            " best_moe_info ", best_moe_info
    print "time ", time.time()-start_tm
    return moe_next_pts, moe_best_pts, moe_means, moe_vars


# Returns a baseline next MOE point of interest.
# This function should be used for establishing a baseline in simulations.
def next_moe_pts_edu_baseline(edu_exp):
    start_tm = time.time()
    moe_next_pts = []
    moe_best_pts = []
    moe_means = []
    moe_vars = []
    for skill_id in xrange(edu_exp.num_skills()):
        next_moe_pt = gp_next_points(
            edu_exp.get_moe_exp(skill_id), num_to_sample=1,
            covariance_info=edu_exp.get_moe_covariance_info(),
            rest_port=edu_exp.get_moe_port())[0]
        best_moe_info = moe_compute_best_pt_info(
            edu_exp.get_moe_exp(skill_id), edu_exp.get_moe_covariance_info())
        moe_next_pts.append(next_moe_pt)
        moe_best_pts.append(best_moe_info[0])
        moe_means.append(best_moe_info[1])
        moe_vars.append(best_moe_info[2])
        print "skill_id ", skill_id, "next next_moe_pt ", \
            next_moe_pt, " best_moe_info ", best_moe_info
    print "time ", time.time()-start_tm
    return moe_next_pts, moe_best_pts, moe_means, moe_vars


# Filter out invalid/unknown/None skill
def get_clean_trajectory(json_traj, skills_key, problems_key):
    clean_observs = []
    clean_observs_skills = []
    for observ_id in xrange(len(json_traj[skills_key])):
        sk = json_traj[skills_key][observ_id]
        if (sk in SKILL_NAME_TO_SKILL_ID):
            clean_observs.append(json_traj[problems_key][observ_id])
            clean_observs_skills.append(SKILL_NAME_TO_SKILL_ID[sk])
    return clean_observs, clean_observs_skills


# Parse the given json inputs.
def parse_trajectories(json_trajectories):
    trajectories = []
    for json_traj in json_trajectories:
        assert(len(json_traj["problems"]) == len(json_traj["problems_skills"]))
        clean_observs, clean_observs_skills = get_clean_trajectory(json_traj, "problems_skills", "problems")
        clean_pretest, clean_test_skills = get_clean_trajectory(json_traj, "test_skills", "pretest")
        clean_posttest, clean_test_skills = get_clean_trajectory(json_traj, "test_skills", "posttest")
        # num_skills, pretest, posttest, test_skills, observs, observs_skills
        traj = Trajectory(
            DEFAULT_NUM_SKILLS, clean_pretest, clean_posttest, clean_test_skills,
            clean_observs, clean_observs_skills)
        traj.printDetailed()
        trajectories.append(traj)
    return trajectories


# Constructs BOEDUExperiment object from the given list of json dictionaries of
# trajectories and previous BO points, then obtains the next point of interest
# from BO (basically calls next_moe_pts_edu()).
# Used in the server for stateless BO, so that no BO-related objects have to
# persist between server sessions.
# The default choices made by this function are to facilitate running
# Tutor BO live experiment 2.
def next_moe_pts_edu_stateless_boexpt2(json_trajectories,
                                       json_bo_params_and_results,
                                       gp_output_file=None,
                                       random_json_trajectories=None):
    assert(len(json_trajectories) == len(json_bo_params_and_results))
    # The following set of choices are for BO live experiment 2.
    objective = "skills_performance_objective_32nd"
    num_resample = 5  # TODO(rika): correct this value if needed after simulations
    # Create a local and thus temporary BOEDUExperiment() object.
    deterministic_policy = True
    edu_exp = BOEDUExperiment(DEFAULT_NUM_SKILLS, MODEL_TYPE_BKT, -1,
                              DEFAULT_MOE_PORT, deterministic_policy)

    # Initialize GPs using json_trajectories, json_bo_params_and_results.
    trajectories = parse_trajectories(json_trajectories)
    if random_json_trajectories is not None:
        random_trajectories = parse_trajectories(random_json_trajectories)
    else:
        random_trajectories = None

    # Parse previously tried BO params and objective results.
    for params in json_bo_params_and_results:
        for skill_name, pdct in params.items():
            skill_id = SKILL_NAME_TO_SKILL_ID[skill_name]
            params = [pdct["pl"], pdct["pt"], pdct["pg"], pdct["ps"], pdct["th"]]
            edu_exp.get_moe_exp(skill_id).historical_data.append_sample_points(
                [[params, pdct["val"], MOE_NOISE_VARIANCE]])
            print "params for skill ", skill_name, " pdct ", pdct, " params ", params

    # Get next interesting points from MOE server.
    rnd_src = np.random.RandomState(None)
    return next_moe_pts_edu(edu_exp, trajectories, objective, rnd_src,
                            max_problems_per_skill=None,
                            num_resample=num_resample,
                            traj_info=None, stockdomain=False,
                            gp_output_file=gp_output_file,
                            random_trajectories=random_trajectories)

