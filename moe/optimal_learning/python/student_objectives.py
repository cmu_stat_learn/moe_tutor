import numpy as np

#
# Experimental objective functions.
#

# Objective for multi-skill trajectories (discussed on Sept 3rd).
# This objective was used to September 2015 live BO experiment. DO NOT MODIFY!
def skills_objective(observs, max_observs, posttest, test_skills, skill_id):
    ts_corr = 0
    ts_denom = 0
    for idx in xrange(len(posttest)):
        if test_skills[idx] == skill_id:
            ts_corr += posttest[idx]
            ts_denom += 1
    t = 1.0*np.sum(posttest)/len(posttest)
    t_adj = 1.0 if t >= 9.0/13.0 else 0.0
    ts = 1.0*ts_corr/ts_denom
    val = (ts + t_adj)/np.sqrt(len(observs)+1.0)
    return 1.0-val  # convert to minimization

#
# Various objectives used for live, simulation and offline experiments.
#

# This objective is used for simulation experiments.
# Objective for multi-skill trajectories adapted from staircase objective.
# In simulations this objective results in thresholds that favor higher posttest
# scores than those found by previously used objectives.
# This objective grows linearly with the number of problems.
# Such objective would help avoiding the optimum of the objective function
# corresponding to very low thresholds. Under posstest_objective for many
# meaningful parameter settings the optimal stopping threshold is [0.2 0.4],
# which usually results in stopping error rate in [40% 60%], this also results
# in very simple optimal policies (e.g. stop after 1 problem, or stop after
# 1 correct answer).
# Optimal thresholds for staircase_objective are usually in [0.6 0.8] and
# for a range of BKT parameter settings allow the optimal policy is non-trivial.
def skills_staircase_objective(observs, max_observs,
                               posttest, test_skills, skill_id):
    desired_posttest = 0.8
    ts_corr = 0
    ts_denom = 0
    for idx in xrange(len(posttest)):
        if test_skills[idx] == skill_id:
            ts_corr += posttest[idx]
            ts_denom += 1
    val = 1.0*(max_observs-len(observs))/(max_observs+1)
    if (1.0*ts_corr/ts_denom) < desired_posttest: val = 0.0
    val = 1.0-val  # convert to minimization
    return val


# Same as above, but with Joe's suggestions for modification.
# An instructor expects the students to start at 30% on the pretest,
# and wants them to achieve 70% mastery on the post test, so the objective
# function is
# (post test score) * (1 - (0.7 - 0.3) * num_probl / max_probl )
# which would only penalize by 0.4/max_problems per problem given.
def skills_soft_staircase_objective(observs, max_observs,
                                    posttest, test_skills, skill_id, desired_posttest=0.7):
    expected_pretest = 0.3  # constants for now, could be modified to be
    ts_corr = 0
    ts_denom = 0
    for idx in xrange(len(posttest)):
        if test_skills[idx] == skill_id:
            ts_corr += posttest[idx]
            ts_denom += 1
    ts = 1.0*ts_corr/ts_denom
    val = ts*(1.0-(desired_posttest-expected_pretest)*len(observs)/max_observs)
    #if ts < desired_posttest: val = 0.0
    val = 1.0-val  # convert to minimization
    return val

# Same as skills_soft_staircase_objective, but with desired_posttest=0.8
def skills_soft_staircase_08_objective(observs, max_observs,
                                       posttest, test_skills, skill_id):
    return skills_soft_staircase_objective(observs, max_observs,
                                           posttest, test_skills, skill_id,
                                           desired_posttest=0.8)


# Same as  skills_soft_staircase_objective() but with additional boost
# in score when the student is doing excellent across skills (>9/13 posttest)
def skills_soft_staircase_adj_objective(observs, max_observs,
                                        posttest, test_skills, skill_id):
    expected_pretest = 0.3  # constants for now, could be modified to be
    desired_posttest = 0.7  # flexible if needed
    ts_corr = 0
    ts_denom = 0
    for idx in xrange(len(posttest)):
        if test_skills[idx] == skill_id:
            ts_corr += posttest[idx]
            ts_denom += 1
    t = 1.0*np.sum(posttest)/len(posttest)
    t_adj = 1.0 if t >= 9.0/13.0 else 0.0
    ts = 1.0*ts_corr/ts_denom
    val = (ts+t_adj)*(1.0-(desired_posttest-expected_pretest)*len(observs)/max_observs)
    val = 1.0-val  # convert to minimization
    return val

# Emma suggested trying an objective with very low weight on number of problems.
def skills_performance_objective(observs, max_observs,
                                 posttest, test_skills, skill_id,
                                 numprobl_alpha=0.125):
    ts_corr = 0
    ts_denom = 0
    for idx in xrange(len(posttest)):
        if test_skills[idx] == skill_id:
            ts_corr += posttest[idx]
            ts_denom += 1
    ts = 1.0*ts_corr/ts_denom
    val = ts/((len(observs)+1)**(numprobl_alpha))
    val = 1.0-val  # convert to minimization
    return val


def skills_performance_objective_16th(observs, max_observs,
                                      posttest, test_skills, skill_id):
    return skills_performance_objective(observs, max_observs,
                                        posttest, test_skills, skill_id,
                                        numprobl_alpha=0.0625)

def skills_performance_objective_32nd(observs, max_observs,
                                      posttest, test_skills, skill_id):
    return skills_performance_objective(observs, max_observs,
                                        posttest, test_skills, skill_id,
                                        numprobl_alpha=0.03125)

# Dummy objective to see if we can optimize just the posttest.
def skills_only_performance_objective(observs, max_observs,
                                      posttest, test_skills, skill_id):
    ts_corr = 0
    ts_denom = 0
    for idx in xrange(len(posttest)):
        if test_skills[idx] == skill_id:
            ts_corr += posttest[idx]
            ts_denom += 1
    ts = 1.0*ts_corr/ts_denom
    val = ts
    val = 1.0-val  # convert to minimization
    return val
