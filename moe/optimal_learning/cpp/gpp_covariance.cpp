/*!
  \file gpp_covariance.cpp
  \rst
  This file contains function definitions for the Covariance, GradCovariance, and HyperparameterGradCovariance member
  functions of CovarianceInterface subclasses.  It also contains a few utilities for computing common mathematical quantities
  and initialization.

  Gradient (spatial and hyperparameter) functions return all derivatives at once because there is substantial shared computation.
  The shared results are by far the most expensive part of gradient computations; they typically involve exponentiation and are
  further at least partially shared with the base covariance computation.

  TODO(GH-132): compute fcn, gradient, and hessian simultaneously for covariance (optionally skipping some terms).

  TODO(GH-129): Check expression simplification of gradients/hessians (esp the latter) for the various covariance functions.
  Current math was done by hand and maybe I missed something.
\endrst*/
//
// NOTE(from rika): This was rather hastily hacked up to add experimental
// support for Quickstart and BBK kernels. Perhaps one day if there is more
// interest in these kernels there will be a need to clean up and add comments.
// All the changes should be inactive as long as USE_KERNEL_TYPE is KERNEL_TYPE::DEFAULT
//
#include "gpp_covariance.hpp"

#include <execinfo.h>  // for backtrace()
#include <stdio.h>

#undef NDEBUG
//#define NDEBUG
#include <cassert>
#include <cmath>

#include <limits>
#include <numeric>   // for inner_product
#include <vector>

#include "gpp_common.hpp"
#include "gpp_exception.hpp"

namespace optimal_learning {

enum KERNEL_TYPE {
  DEFAULT = 0,
  QUICKSTART_TRAJ = 1,
  QUICKSTART_Y = 2,
  BBK = 3
};
const int USE_KERNEL_TYPE = KERNEL_TYPE::DEFAULT;
//const int USE_KERNEL_TYPE = KERNEL_TYPE::QUICKSTART_TRAJ;
//const int USE_KERNEL_TYPE = KERNEL_TYPE::QUICKSTART_Y;
//const int USE_KERNEL_TYPE = KERNEL_TYPE::BBK;
const int BBK_NUM_TRAJ = 1;  // number of trajectories supplied for BBK
const bool MOUNTAIN_CAR = true;  // whether domain is mountain car (tutor if false)
const bool DEBUG_COVAR = false;  // print DEBUG_COVAR statements
const int MOUNTAIN_CAR_TRAJ_STEP_DIM = 9;  // 8 features + action
const int MOUNTAIN_CAR_SCALING_COEF = 10;  // scaling coefficient for policy params

namespace {

/*!\rst
  Computes ``\sum_{i=0}^{dim} (p1_i - p2_i) / W_i * (p1_i - p2_i)``, ``p1, p2 = point 1 & 2``; ``W = weight``.
  Equivalent to ``\|p1 - p2\|_2`` if all entries of W are 1.0.

  \param
    :point_one[size]: the vector p1
    :point_two[size]: the vector p2
    :weights[size]: the vector W, i.e., the scaling to apply to each term of the norm
    :size: number of dimensions in point
  \return
    the weighted ``L_2``-norm of the vector difference ``p1 - p2``.
\endrst*/
OL_PURE_FUNCTION OL_NONNULL_POINTERS OL_WARN_UNUSED_RESULT double
NormSquaredWithInverseWeights(double const * restrict point_one, double const * restrict point_two,
                              double const * restrict weights, int size) noexcept {
  // calculates the one norm of two vectors (point_one and point_two of size size)
  double norm = 0.0;

  for (int i = 0; i < size; ++i) {
    norm += Square((point_one[i] - point_two[i]))/weights[i];
  }
  return norm;
}

// TODO(rika): add comments.
OL_PURE_FUNCTION OL_NONNULL_POINTERS OL_WARN_UNUSED_RESULT double
LRThreshAction(double const * restrict policy_vec, double const * restrict traj,
               int dim, int traj_step_dim) noexcept {
  double pdot = 0.0;
  for (int k=0; k<dim-1; ++k) { pdot += policy_vec[k]*traj[k]; }
  double thresh = policy_vec[dim-1];
  double pleaned = 1.0/(1.0+std::exp(-pdot));
  if (KERNEL_TYPE::BBK == USE_KERNEL_TYPE) {  // stochastic policy
      return pleaned;
  } else {  // deterministic policy
      return (pleaned > thresh) ? 1 : 0;
  }
}

// TODO(rika): add comments.
OL_PURE_FUNCTION OL_NONNULL_POINTERS OL_WARN_UNUSED_RESULT double
MountainCarAction(double const * restrict policy_vec, double const * restrict traj,
                  int dim, int traj_step_dim) noexcept {
  //leftPolicy = np.dot(policy_parameters[:8], state_features)
  //rightPolicy = np.dot(policy_parameters[8:], state_features)
  double lpdot = 0.0;
  double rpdot = 0.0;
  for (int k=0; k<dim; ++k) {
      if (k < traj_step_dim-1) {
          lpdot += MOUNTAIN_CAR_SCALING_COEF*policy_vec[k]*traj[k];
      } else {
          rpdot += MOUNTAIN_CAR_SCALING_COEF*policy_vec[k]*traj[k-(traj_step_dim-1)];
      }
  }
  double maxPolicy = (lpdot<rpdot) ? rpdot:lpdot;
  double minPolicy = (lpdot<rpdot) ? lpdot:rpdot;
  double prob_left = std::exp(lpdot - maxPolicy - log(1 + std::exp(minPolicy - maxPolicy)));
  if (KERNEL_TYPE::BBK == USE_KERNEL_TYPE) {  // stochastic policy
      // Return probability of action actually taken.
      int a_t = traj[traj_step_dim-1];
      if (DEBUG_COVAR) printf("a_t=%d prob_left=%0.4f lpdot=%0.4f rpdot=%0.4f\n", a_t, prob_left, lpdot, rpdot);
      return (a_t == -1 ) ? prob_left : (1.0-prob_left);
  } else {  // deterministic policy
      return (prob_left > 0.5) ? -1 : 1;
  }
}

// TODO(rika): add comments.
OL_PURE_FUNCTION OL_NONNULL_POINTERS OL_WARN_UNUSED_RESULT double
ComputeAction(double const * restrict policy_vec, double const * restrict traj,
              int dim, int traj_step_dim) noexcept {
  return MOUNTAIN_CAR ? MountainCarAction(policy_vec, traj, dim, traj_step_dim)
      : LRThreshAction(policy_vec, traj, dim, traj_step_dim);
}

// TODO(rika): add comments. Key function for Quickstart kernel.
OL_PURE_FUNCTION OL_NONNULL_POINTERS OL_WARN_UNUSED_RESULT double
QuickstartKernelCovariance(double const * restrict point_one, double const * restrict point_two, int dim,
                           double const * restrict weights, double alpha,
                           int traj_dim, int num_traj, double const * restrict trajectories,
                           double norm_val, double prior, double * cov_l2_adjust) noexcept {
  // TODO(rika): perhaps add caching?
  int traj_step_dim = MOUNTAIN_CAR ? MOUNTAIN_CAR_TRAJ_STEP_DIM : dim;
  int traj_steps = traj_dim/traj_step_dim;
  if (DEBUG_COVAR) {
      printf("Quickstart trajectories (traj_dim=%d, dim_=%d num_traj=%d):\n",
             traj_dim, dim, num_traj);
      printf("point_one: ");
      for (int k=0; k<dim; ++k) { printf("%0.4f ", point_one[k]); }
      printf(" point_two: ");
      for (int k=0; k<dim; ++k) { printf("%0.4f ", point_two[k]); }
      printf("\n");
      fflush(stdout);
  }

  int num_agree = 0;
  int tot_agree_possible = 0;
  std::vector<double> pt1_vals(num_traj, 1.0);
  std::vector<double> pt2_vals(num_traj, 1.0);
  std::vector<double> traj_weights(num_traj, 1.0);
  for (int i=0; i<num_traj; ++i) {
      bool pt1_stop_found = false;
      bool pt2_stop_found = false;
      // For each step on the trajectory compute the action induced by point_one and point_two.
      for (int j=0; j<traj_steps; ++j) {
          if (std::isnan(trajectories[i*traj_dim+j*traj_step_dim])) continue;  // done with trajectory
          int a1 = ComputeAction(point_one, trajectories+(i*traj_dim+j*traj_step_dim), dim, traj_step_dim);
          int a2 = ComputeAction(point_two, trajectories+(i*traj_dim+j*traj_step_dim), dim, traj_step_dim);
          if (a1 == a2) { num_agree++; }
          tot_agree_possible++;
          if (a1 == 1 && !pt1_stop_found) {
              pt1_stop_found = true;
              pt1_vals[i] = j/traj_steps;
          }
          if (a2 == 1 && !pt2_stop_found) {
              pt2_stop_found = true;
              pt2_vals[i] = j/traj_steps;
          }
          // Print data for the current step.
          if (DEBUG_COVAR) for (int k=0; k<traj_step_dim; ++k) { printf("%0.4f ", trajectories[i*traj_dim+j*traj_step_dim+k]); }
          if (DEBUG_COVAR) printf("\ntraj=%d step=%d : a1=%d a2=%d\n", i, j, a1, a2);
      }
      if (DEBUG_COVAR) printf(" => traji=%d num_agree=%d tot_agree_possible=%d\n", i, num_agree, tot_agree_possible);
  }
  if (DEBUG_COVAR && (KERNEL_TYPE::QUICKSTART_Y == USE_KERNEL_TYPE)) {
      printf("pt1_vals:");
      for (int i=0; i<num_traj; ++i) { printf("%0.4f ", pt1_vals[i]); }
      printf("\npt2_vals:");
      for (int i=0; i<num_traj; ++i) { printf("%0.4f ", pt2_vals[i]); }
      printf("\n");
  }
  double posterior;
  if (KERNEL_TYPE::QUICKSTART_Y == USE_KERNEL_TYPE) {
      double objective_dist = 0.0;
      for (int i=0; i<num_traj; ++i) { objective_dist += fabs(pt1_vals[i]-pt2_vals[i]); }
      objective_dist /= num_traj;  // normalized to be in [0,1]
      posterior = 0.5*prior+0.5*(1.0-objective_dist);
      if (DEBUG_COVAR) printf("norm_val=%0.4f objective_dist=%0.4f\n", norm_val, objective_dist);
  } else {
      double a = ceil(prior*traj_steps);  // the closer the points, the larger the prior, the larger a gets
      double b = traj_steps-a;
      posterior = (a+num_agree)/(traj_steps+tot_agree_possible);
      if (DEBUG_COVAR) {
          printf("norm_val=%0.4f a=%0.4f b=%0.4f num_agree=%d tot=%d \n",
                 norm_val, a, b, num_agree, tot_agree_possible);
       }
  }
  assert(posterior > 0);
  double k = posterior/prior;
  *cov_l2_adjust = (-2.0*log(k)+norm_val)/norm_val;
  if (*cov_l2_adjust < 0.0) { *cov_l2_adjust = 0.0; }  // largest adjustment is making points have distance 0
  if (DEBUG_COVAR) { printf("posterior=%0.4f prior=%0.4f cov_l2_adjust=%0.4f\n", posterior, prior, *cov_l2_adjust); }
  return alpha*posterior;
}

// Key function for BBK kernel. Followed the math presented in the paper.
// Wilson's MATLAB code could have different constants/tricks, but it seems
// fair to implement the description presented in the paper.
OL_PURE_FUNCTION OL_NONNULL_POINTERS OL_WARN_UNUSED_RESULT double
BBKCovariance(double const * restrict point_one, double const * restrict point_two, int dim,
              double const * restrict weights, double alpha,
              int traj_dim, int num_traj, double const * restrict trajectories, int p1_traj_id, int p2_traj_id,
              double norm_val, double prior, double *cov_l2_adjust) noexcept {
  if (DEBUG_COVAR) {
      printf("BBK trajectories (traj_dim=%d, dim_=%d num_traj=%d):\n",
             traj_dim, dim, num_traj);
      printf("point_one: ");
      for (int k=0; k<dim; ++k) { printf("%0.4f ", point_one[k]); }
      printf(" point_two: ");
      for (int k=0; k<dim; ++k) { printf("%0.4f ", point_two[k]); }
      printf("\n");
      fflush(stdout);
  }
  int traj_step_dim = MOUNTAIN_CAR ? MOUNTAIN_CAR_TRAJ_STEP_DIM : dim;
  int traj_steps = traj_dim/traj_step_dim;
  double bbk_dist = 0.0;
  if (DEBUG_COVAR) printf("p1_traj_id=%d p2_traj_id=%d\n", p1_traj_id, p2_traj_id);
  assert(p1_traj_id >= 0);
  // Sum over BBK_NUM_TRAJ for point_one
  for (int i=p1_traj_id*BBK_NUM_TRAJ; i<p1_traj_id*BBK_NUM_TRAJ+BBK_NUM_TRAJ; ++i) {
      if (DEBUG_COVAR) printf("p1 traj i=%d\n", i);
      // For each step on the trajectory compute the action induced by point_one and point_two.
      double log_dist = 0.0;
      for (int j=0; j<traj_steps; ++j) {
          if (std::isnan(trajectories[i*traj_dim+j*traj_step_dim])) continue;  // done with trajectory
          double p1a0 = ComputeAction(point_one, trajectories+(i*traj_dim+j*traj_step_dim), dim, traj_step_dim);
          double p2a0 = ComputeAction(point_two, trajectories+(i*traj_dim+j*traj_step_dim), dim, traj_step_dim);
          if (DEBUG_COVAR) printf("p1a0=%0.4f p2a0=%0.4f\n", p1a0, p2a0);
          assert(p1a0 > 0.0);
          assert(p2a0 > 0.0);
          log_dist += log(p1a0/p2a0);
      }
      if (DEBUG_COVAR) printf("bbk_dist=%0.4f log_dist=%0.4f\n", bbk_dist, log_dist);
      bbk_dist += log_dist;
  }
  // Sum over BBK_NUM_TRAJ for point_two.
  // Use importance sampling if there are no trajectories for point_two.
  if (p2_traj_id < 0) p2_traj_id = p1_traj_id;
  for (int i=p2_traj_id*BBK_NUM_TRAJ; i<p2_traj_id*BBK_NUM_TRAJ+BBK_NUM_TRAJ; ++i) {
      if (DEBUG_COVAR) printf("p2 traj i=%d\n", i);
      double log_dist = 0.0;
      // For each step on the trajectory compute the action induced by point_one and point_two.
      for (int j=0; j<traj_steps; ++j) {
          if (std::isnan(trajectories[i*traj_dim+j*traj_step_dim])) continue;  // done with trajectory
          double p1a0 = ComputeAction(point_one, trajectories+(i*traj_dim+j*traj_step_dim), dim, traj_step_dim);
          double p2a0 = ComputeAction(point_two, trajectories+(i*traj_dim+j*traj_step_dim), dim, traj_step_dim);
          if (DEBUG_COVAR) printf("p1a0=%0.4f p2a0=%0.4f\n", p1a0, p2a0);
          assert(p1a0 > 0.0);
          assert(p2a0 > 0.0);
          log_dist += log(p2a0/p1a0);
      }
      double mult = 1.0;
      if (p1_traj_id == p2_traj_id) { mult = std::exp(log_dist); }  // importance sampling
      if (DEBUG_COVAR) printf("bbk_dist=%0.4f log_dist=%0.4f mult=%0.4f\n", bbk_dist, log_dist, mult);
      bbk_dist += mult*log_dist;
  }
  // bbk_dist can be quite large for long trajectories and it is not clear to me how Wilson
  // handled this problem. Looking at the MATLAB code seems like a strange function is called
  // to compute the likelihood array for taking actions on a path.
  double posterior = std::exp(-0.5*bbk_dist);
  assert(posterior > 0);
  double k = posterior/prior;
  *cov_l2_adjust = (-2.0*log(k)+norm_val)/norm_val;
  if (DEBUG_COVAR) { printf("posterior=%0.4f prior=%0.4f cov_l2_adjust=%0.4f\n", posterior, prior, *cov_l2_adjust); }
  return alpha*posterior;
}

// TODO(rika): add comments. Key function for modified kernels.
OL_PURE_FUNCTION OL_NONNULL_POINTERS OL_WARN_UNUSED_RESULT double
AdjustedCovariance(double const * restrict point_one, double const * restrict point_two, int dim,
                   double const * restrict weights, double alpha,
                   int traj_dim, int num_traj, double const * restrict trajectories,
                   int p1_traj_id, int p2_traj_id, double * cov_l2_adjust) noexcept {
  assert(USE_KERNEL_TYPE != KERNEL_TYPE::DEFAULT);
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, weights, dim);
  assert(!std::isnan(norm_val));
  assert(std::isfinite(norm_val));
  double prior = std::exp(-0.5*norm_val);  // how much kernel influence to give a priori
  assert(prior > 0);
  if (norm_val == 0) { return alpha*prior; }
  if (traj_dim==0 or num_traj==0) { return alpha*prior; }

  if (KERNEL_TYPE::BBK == USE_KERNEL_TYPE) {
      return BBKCovariance(point_one, point_two, dim, weights, alpha,
                           traj_dim, num_traj, trajectories, p1_traj_id, p2_traj_id,
                           norm_val, prior, cov_l2_adjust);
  } else {
      return QuickstartKernelCovariance(point_one, point_two, dim, weights, alpha,
                                        traj_dim, num_traj, trajectories,
                                        norm_val, prior, cov_l2_adjust);
  }
}

/*!\rst
  Validate and initialize covariance function data (sizes, hyperparameters).

  \param
    :dim: the number of spatial dimensions
    :alpha: the hyperparameter \alpha, (e.g., signal variance, \sigma_f^2)
    :lengths_in: the input length scales, one per spatial dimension
    :lengths_sq[dim]: pointer to an array of at least dim double
  \output
    :lengths_sq[dim]: first dim entries overwritten with the square of the entries of lengths_in
\endrst*/
OL_NONNULL_POINTERS void InitializeCovariance(int dim, double alpha, const std::vector<double>& lengths_in,
                                              double * restrict lengths_sq) {
  // validate inputs
  if (dim < 0) {
    OL_THROW_EXCEPTION(LowerBoundException<int>, "Negative spatial dimension.", dim, 0);
  }

  if (static_cast<unsigned>(dim) != lengths_in.size()) {
    OL_THROW_EXCEPTION(InvalidValueException<int>, "dim (truth) and length vector size do not match.", lengths_in.size(), dim);
  }

  if (alpha <= 0.0) {
    OL_THROW_EXCEPTION(LowerBoundException<double>, "Invalid hyperparameter (alpha).", alpha, std::numeric_limits<double>::min());
  }

  // fill lengths_sq array
  for (int i = 0; i < dim; ++i) {
    lengths_sq[i] = Square(lengths_in[i]);
    if (unlikely(lengths_in[i] <= 0.0)) {
      OL_THROW_EXCEPTION(LowerBoundException<double>, "Invalid hyperparameter (length).", lengths_in[i], std::numeric_limits<double>::min());
    }
  }
}

}  // end unnamed namespace

void SquareExponential::Initialize() {
  InitializeCovariance(dim_, alpha_, lengths_, lengths_sq_.data());
}

SquareExponential::SquareExponential(int dim, double alpha, std::vector<double> lengths)
    : dim_(dim), alpha_(alpha), lengths_(lengths), lengths_sq_(dim) {
  Initialize();
}

SquareExponential::SquareExponential(int dim, double alpha, double const * restrict lengths)
    : SquareExponential(dim, alpha, std::vector<double>(lengths, lengths + dim)) {
}

SquareExponential::SquareExponential(int dim, double alpha, double length)
    : SquareExponential(dim, alpha, std::vector<double>(dim, length)) {
}

SquareExponential::SquareExponential(const SquareExponential& OL_UNUSED(source)) = default;

/*
  Square Exponential adjusted with trajectory data.
*/
double SquareExponential::Covariance(double const * restrict point_one, double const * restrict point_two,
                                     int traj_dim, int num_traj, double const * restrict trajectories,
                                     int p1_traj_id, int p2_traj_id) const noexcept {
  if (KERNEL_TYPE::DEFAULT == USE_KERNEL_TYPE) return Covariance(point_one, point_two);
  double cov_l2_adjust;
  return AdjustedCovariance(point_one, point_two, dim_, lengths_sq_.data(), alpha_,
                            traj_dim, num_traj, trajectories, p1_traj_id, p2_traj_id, &cov_l2_adjust);
}

/*
  Square Exponential: ``cov(x_1, x_2) = \alpha * \exp(-1/2 * ((x_1 - x_2)^T * L * (x_1 - x_2)) )``
*/
double SquareExponential::Covariance(double const * restrict point_one, double const * restrict point_two) const noexcept {
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  return alpha_*std::exp(-0.5*norm_val);
}

/*
  Gradient of Square Exponential (wrt ``x_1``):
  ``\pderiv{cov(x_1, x_2)}{x_{1,i}} = (x_{2,i} - x_{1,i}) / L_{i}^2 * cov(x_1, x_2)``
*/
void SquareExponential::GradCovariance(double const * restrict point_one, double const * restrict point_two,
                                       int traj_dim, int num_traj, double const * restrict trajectories,
                                       int p1_traj_id, int p2_traj_id, double * restrict grad_cov) const noexcept {
  double cov;
  double l1_adjust = 1.0;
  // Revert to baseline SquareExponential if traj_dim==0 or num_traj==0
  if (traj_dim==0 or num_traj==0) {
      cov = Covariance(point_one, point_two);
  } else {
      double cov_l2_adjust;
      cov = AdjustedCovariance(point_one, point_two, dim_, lengths_sq_.data(), alpha_,
                               traj_dim, num_traj, trajectories, p1_traj_id, p2_traj_id, &cov_l2_adjust);
      l1_adjust = sqrt(cov_l2_adjust);
  }
  for (int i = 0; i < dim_; ++i) {
    grad_cov[i] = (point_two[i] - point_one[i])*l1_adjust/lengths_sq_[i]*cov;
  }
}

/*
  Gradient of Square Exponential (wrt hyperparameters (``alpha, L``)):
  ``\pderiv{cov(x_1, x_2)}{\theta_0} = cov(x_1, x_2) / \theta_0``
  ``\pderiv{cov(x_1, x_2)}{\theta_0} = [(x_{1,i} - x_{2,i}) / L_i]^2 / L_i * cov(x_1, x_2)``
  Note: ``\theta_0 = \alpha`` and ``\theta_{1:d} = L_{0:d-1}``
*/
void SquareExponential::HyperparameterGradCovariance(double const * restrict point_one, double const * restrict point_two,
                                                     double * restrict grad_hyperparameter_cov) const noexcept {
  const double cov = Covariance(point_one, point_two);

  // deriv wrt alpha does not have the same form as the length terms, special case it
  grad_hyperparameter_cov[0] = cov/alpha_;
  for (int i = 0; i < dim_; ++i) {
    grad_hyperparameter_cov[i+1] = cov*Square((point_one[i] - point_two[i])/lengths_[i])/lengths_[i];
  }
}

void SquareExponential::HyperparameterHessianCovariance(double const * restrict point_one, double const * restrict point_two,
                                                        double * restrict hessian_hyperparameter_cov) const noexcept {
  const double cov = Covariance(point_one, point_two);
  const int num_hyperparameters = GetNumberOfHyperparameters();
  std::vector<double> grad_hyper_cov(num_hyperparameters);
  std::vector<double> componentwise_scaled_distance(num_hyperparameters);
  HyperparameterGradCovariance(point_one, point_two, grad_hyper_cov.data());

  for (int i = 0; i < dim_; ++i) {
    // note the indexing is shifted by one here so that we can iterate over componentwise_scaled_distance with the
    // same index that is used for hyperparameter indexing
    componentwise_scaled_distance[i+1] = Square((point_one[i] - point_two[i])/lengths_[i])/lengths_[i];  // (x1_i - x2_i)^2/l_i^3
  }

  double const * restrict hessian_hyperparameter_cov_row = hessian_hyperparameter_cov + 1;  // used to index through rows
  // first column of hessian, derivatives of pderiv{cov}{\theta_0} with respect to \theta_i
  // \theta_0 is alpha, the scaling factor; its derivatives are fundamentally different in form than those wrt length scales;
  // hence they are split out of the loop
  hessian_hyperparameter_cov[0] = 0.0;
  for (int i = 1; i < num_hyperparameters; ++i) {
    // this is simply pderiv{cov}{\theta_i}/alpha, i = 1..dim+1
    hessian_hyperparameter_cov[i] = grad_hyper_cov[0]*componentwise_scaled_distance[i];
  }
  hessian_hyperparameter_cov += num_hyperparameters;

  // remaining columns of the hessian: derivatives with respect to pderiv{cov}{\theta_j} for j = 1..dim+1 (length scales)
  // terms come from straightforward differentiation of HyperparameterGradCovariance() expressions; no simplification needed
  for (int j = 1; j < num_hyperparameters; ++j) {
    // copy j-th column from j-th row, which has already been computed
    for (int i = 0; i < j; ++i) {
      hessian_hyperparameter_cov[i] = hessian_hyperparameter_cov_row[0];
      hessian_hyperparameter_cov_row += num_hyperparameters;
    }
    hessian_hyperparameter_cov_row -= j*num_hyperparameters;  // reset row for next iteration

    // on diagonal component has extra terms since normally dx_i/dx_j = 0 except for i == j
    // the RHS terms are only read from already-computed or copied components
    hessian_hyperparameter_cov[j] = cov*(Square(componentwise_scaled_distance[j]) - 3.0*componentwise_scaled_distance[j]/lengths_[j-1]);
    // remaining off-digaonal terms
    for (int i = j+1; i < num_hyperparameters; ++i) {
      hessian_hyperparameter_cov[i] = cov*componentwise_scaled_distance[i]*componentwise_scaled_distance[j];
    }

    hessian_hyperparameter_cov += num_hyperparameters;
    hessian_hyperparameter_cov_row += 1;
  }
}

CovarianceInterface * SquareExponential::Clone() const {
  return new SquareExponential(*this);
}

namespace {

// computes ||p1 - p2||_2 if all entries of L == 1
OL_PURE_FUNCTION OL_NONNULL_POINTERS OL_WARN_UNUSED_RESULT double
NormSquaredWithConstInverseWeights(double const * restrict point_one, double const * restrict point_two,
                                   double weight, int size) noexcept {
  // calculates the one norm of two vectors (point_one and point_two of size size)
  double norm = 0.0;

  for (int i = 0; i < size; ++i) {
    norm += Square(point_one[i] - point_two[i]);
  }
  return norm/weight;
}

}  // end unnamed namespace

SquareExponentialSingleLength::SquareExponentialSingleLength(int dim, double alpha, double length)
    : dim_(dim), alpha_(alpha), length_(length), length_sq_(length*length) {
}

SquareExponentialSingleLength::SquareExponentialSingleLength(int dim, double alpha, double const * restrict length)
    : SquareExponentialSingleLength(dim, alpha, length[0]) {
}

SquareExponentialSingleLength::SquareExponentialSingleLength(int dim, double alpha, std::vector<double> length)
    : SquareExponentialSingleLength(dim, alpha, length[0]) {
}

SquareExponentialSingleLength::SquareExponentialSingleLength(const SquareExponentialSingleLength& OL_UNUSED(source)) = default;

double SquareExponentialSingleLength::Covariance(double const * restrict point_one, double const * restrict point_two, int traj_dim,
                                                 int num_traj, double const * restrict trajectories,
                                                 int p1_traj_id, int p2_traj_id) const noexcept {
  printf("SquareExponentialSingleLength::Covariance()\n");
  assert(false);  // not implemented and not valid for this kernel
  return 0.0;
}
double SquareExponentialSingleLength::Covariance(double const * restrict point_one,
                                                 double const * restrict point_two) const noexcept {
  const double norm_val = NormSquaredWithConstInverseWeights(point_one, point_two, length_sq_, dim_);
  return alpha_*std::exp(-0.5*norm_val);
}

void SquareExponentialSingleLength::GradCovariance(double const * restrict point_one, double const * restrict point_two,
                                                   int traj_dim, int num_traj, double const * restrict trajectories,
                                                   int p1_traj_id, int p2_traj_id,
                                                   double * restrict grad_cov) const noexcept {
  const double cov = Covariance(point_one, point_two)/length_sq_;

  for (int i = 0; i < dim_; ++i) {
    grad_cov[i] = (point_two[i] - point_one[i])*cov;
  }
}

void SquareExponentialSingleLength::HyperparameterGradCovariance(double const * restrict point_one,
                                                                 double const * restrict point_two,
                                                                 double * restrict grad_hyperparameter_cov) const noexcept {
  const double cov = Covariance(point_one, point_two);
  const double norm_val = NormSquaredWithConstInverseWeights(point_one, point_two, length_sq_, dim_);

  grad_hyperparameter_cov[0] = cov/alpha_;
  grad_hyperparameter_cov[1] = cov*norm_val/length_;
}

void SquareExponentialSingleLength::HyperparameterHessianCovariance(double const * restrict point_one,
                                                                    double const * restrict point_two,
                                                                    double * restrict hessian_hyperparameter_cov) const noexcept {
  const double cov = Covariance(point_one, point_two);
  const double scaled_norm_val = NormSquaredWithConstInverseWeights(point_one, point_two, length_sq_, dim_)/length_;

  hessian_hyperparameter_cov[0] = 0.0;
  hessian_hyperparameter_cov[1] = cov/alpha_*scaled_norm_val;
  hessian_hyperparameter_cov[2] = hessian_hyperparameter_cov[1];
  hessian_hyperparameter_cov[3] = cov*(Square(scaled_norm_val) - 3.0*scaled_norm_val/length_);
}

CovarianceInterface * SquareExponentialSingleLength::Clone() const {
  return new SquareExponentialSingleLength(*this);
}

void MaternNu1p5::Initialize() {
  InitializeCovariance(dim_, alpha_, lengths_, lengths_sq_.data());
}

MaternNu1p5::MaternNu1p5(int dim, double alpha, std::vector<double> lengths)
    : dim_(dim), alpha_(alpha), lengths_(lengths), lengths_sq_(dim) {
  Initialize();
}

MaternNu1p5::MaternNu1p5(int dim, double alpha, double const * restrict lengths)
    : MaternNu1p5(dim, alpha, std::vector<double>(lengths, lengths + dim)) {
}

MaternNu1p5::MaternNu1p5(int dim, double alpha, double length)
    : MaternNu1p5(dim, alpha, std::vector<double>(dim, length)) {
}

MaternNu1p5::MaternNu1p5(const MaternNu1p5& OL_UNUSED(source)) = default;

double MaternNu1p5::Covariance(double const * restrict point_one, double const * restrict point_two, int traj_dim,
                               int num_traj, double const * restrict trajectories,
                               int p1_traj_id, int p2_traj_id) const noexcept {
	printf("MaternNu1p5::Covariance()\n");
  assert(false);  // not implemented and not valid for this kernel
  return 0.0;
}
double MaternNu1p5::Covariance(double const * restrict point_one, double const * restrict point_two) const noexcept {
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  const double matern_arg = kSqrt3 * std::sqrt(norm_val);

  return alpha_*(1.0 + matern_arg)*std::exp(-matern_arg);
}

void MaternNu1p5::GradCovariance(double const * restrict point_one, double const * restrict point_two,
                                 int traj_dim, int num_traj, double const * restrict trajectories,
                                 int p1_traj_id, int p2_traj_id, double * restrict grad_cov) const noexcept {
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  if (norm_val == 0.0) {
    std::fill(grad_cov, grad_cov + dim_, 0.0);
    return;
  }
  const double matern_arg = kSqrt3 * std::sqrt(norm_val);
  const double exp_part = std::exp(-matern_arg);

  // terms from differentiating Covariance wrt spatial dimensions; since exp(x) is the derivative's identity, some cancellation of
  // analytic 0s is possible (and desired since it reduces compute-time and is more accurate)
  for (int i = 0; i < dim_; ++i) {
    const double dr_dxi = (point_one[i] - point_two[i])/std::sqrt(norm_val)/lengths_sq_[i];
    grad_cov[i] = -3.0*alpha_*dr_dxi*exp_part*std::sqrt(norm_val);
  }
}

void MaternNu1p5::HyperparameterGradCovariance(double const * restrict point_one, double const * restrict point_two,
                                               double * restrict grad_hyperparameter_cov) const noexcept {
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  if (norm_val == 0.0) {
    grad_hyperparameter_cov[0] = 1.0;
    std::fill(grad_hyperparameter_cov+1, grad_hyperparameter_cov + 1+dim_, 0.0);
    return;
  }
  const double matern_arg = kSqrt3 * std::sqrt(norm_val);
  const double exp_part = std::exp(-matern_arg);

  // deriv wrt alpha does not have the same form as the length terms, special case it
  grad_hyperparameter_cov[0] = (1.0 + matern_arg) * exp_part;
  // terms from differentiating Covariance wrt spatial dimensions; since exp(x) is the derivative's identity, some cancellation of
  // analytic 0s is possible (and desired since it reduces compute-time and is more accurate)
  for (int i = 0; i < dim_; ++i) {
    const double dr_dleni = -Square((point_one[i] - point_two[i])/lengths_[i])/std::sqrt(norm_val)/lengths_[i];
    grad_hyperparameter_cov[i+1] = -3.0*alpha_*dr_dleni*exp_part*std::sqrt(norm_val);
  }
}

void MaternNu1p5::HyperparameterHessianCovariance(double const * restrict point_one, double const * restrict point_two,
                                                  double * restrict hessian_hyperparameter_cov) const noexcept {
  const int num_hyperparameters = GetNumberOfHyperparameters();
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  if (norm_val == 0.0) {
    std::fill(hessian_hyperparameter_cov, hessian_hyperparameter_cov + Square(num_hyperparameters), 0.0);
    return;
  }

  const double sqrt_norm_val = std::sqrt(norm_val);
  const double matern_arg = kSqrt3 * sqrt_norm_val;
  const double exp_part = std::exp(-matern_arg);

  // precompute deriv of r wrt length hyperparameters, where r is NormSquaredWithInverseWeights
  std::vector<double> dr_dlen(num_hyperparameters);
  for (int i = 0; i < dim_; ++i) {
    // note the indexing is shifted by one here so that we can iterate over componentwise_scaled_distance with the
    // same index that is used for hyperparameter indexing
    dr_dlen[i+1] = -Square((point_one[i] - point_two[i])/lengths_[i])/lengths_[i]/sqrt_norm_val;  // -1/r*(x1_i - x2_i)^2/l_i^3
  }

  double const * restrict hessian_hyperparameter_cov_row = hessian_hyperparameter_cov + 1;  // used to index through rows
  // first column of hessian, derivatives of pderiv{cov}{\theta_0} with respect to \theta_i
  // \theta_0 is alpha, the scaling factor; its derivatives are fundamentally different than those wrt length scales; hence
  // they are split out of the loop
  // deriv wrt alpha does not have the same form as the length terms, special case it
  hessian_hyperparameter_cov[0] = 0.0;
  for (int i = 1; i < num_hyperparameters; ++i) {
    // this is simply pderiv{cov}{\theta_i}/alpha, i = 1..dim+1
    hessian_hyperparameter_cov[i] = -3.0*dr_dlen[i]*exp_part*sqrt_norm_val;
  }
  hessian_hyperparameter_cov += num_hyperparameters;

  // remaining columns of the hessian: derivatives with respect to pderiv{cov}{\theta_j} for j = 1..dim+1 (length scales)
  // terms from differentiating HyperparameterGradCovariance wrt spatial dimensions; since exp(x) is the derivative's identity,
  // some cancellation of analytic 0s is possible (and desired since it reduces compute-time and is more accurate)
  for (int j = 1; j < num_hyperparameters; ++j) {
    // copy j-th column from j-th row, which has already been computed
    for (int i = 0; i < j; ++i) {
      hessian_hyperparameter_cov[i] = hessian_hyperparameter_cov_row[0];
      hessian_hyperparameter_cov_row += num_hyperparameters;
    }
    hessian_hyperparameter_cov_row -= j*num_hyperparameters;  // reset row for next iteration

    // on diagonal component has extra terms since normally dx_i/dx_j = 0 except for i == j
    // the RHS terms are only read from already-computed or copied components
    hessian_hyperparameter_cov[j] = -kSqrt3*alpha_*hessian_hyperparameter_cov[0]*dr_dlen[j] +
        alpha_*(-3.0/lengths_[j-1])*hessian_hyperparameter_cov[0];
    // remaining off-digaonal terms
    for (int i = j+1; i < num_hyperparameters; ++i) {
      hessian_hyperparameter_cov[i] = -kSqrt3*alpha_*hessian_hyperparameter_cov[0]*dr_dlen[i];
    }

    hessian_hyperparameter_cov += num_hyperparameters;
    hessian_hyperparameter_cov_row += 1;
  }
}

CovarianceInterface * MaternNu1p5::Clone() const {
  return new MaternNu1p5(*this);
}

void MaternNu2p5::Initialize() {
  InitializeCovariance(dim_, alpha_, lengths_, lengths_sq_.data());
}

MaternNu2p5::MaternNu2p5(int dim, double alpha, std::vector<double> lengths)
    : dim_(dim), alpha_(alpha), lengths_(lengths), lengths_sq_(dim) {
  Initialize();
}

MaternNu2p5::MaternNu2p5(int dim, double alpha, double const * restrict lengths)
    : MaternNu2p5(dim, alpha, std::vector<double>(lengths, lengths + dim)) {
}

MaternNu2p5::MaternNu2p5(int dim, double alpha, double length)
    : MaternNu2p5(dim, alpha, std::vector<double>(dim, length)) {
}

MaternNu2p5::MaternNu2p5(const MaternNu2p5& OL_UNUSED(source)) = default;
double MaternNu2p5::Covariance(double const * restrict point_one, double const * restrict point_two, int traj_dim,
                               int num_traj, double const * restrict trajectory, int p1_traj_id, int p2_traj_id) const noexcept {
  printf("MaternNu2p5::Covariance()\n");
  assert(false);  // not implemented and not valid for this kernel
  return 0.0;
}
double MaternNu2p5::Covariance(double const * restrict point_one, double const * restrict point_two) const noexcept {
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  const double matern_arg = kSqrt5 * std::sqrt(norm_val);

  return alpha_*(1.0 + matern_arg + 5.0/3.0*norm_val)*std::exp(-matern_arg);
}

void MaternNu2p5::GradCovariance(double const * restrict point_one, double const * restrict point_two,
                                 int traj_dim, int num_traj, double const * restrict trajectory,
                                 int p1_traj_id, int p2_traj_id, double * restrict grad_cov) const noexcept {
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  if (norm_val == 0.0) {
    std::fill(grad_cov, grad_cov + dim_, 0.0);
    return;
  }
  const double matern_arg = kSqrt5 * std::sqrt(norm_val);
  const double poly_part = matern_arg + 5.0/3.0*norm_val;
  const double exp_part = std::exp(-matern_arg);

  for (int i = 0; i < dim_; ++i) {
    const double dr2_dxi = 2.0*(point_one[i] - point_two[i])/lengths_sq_[i];
    const double dr_dxi = 0.5*dr2_dxi/std::sqrt(norm_val);
    grad_cov[i] = alpha_*exp_part*(5.0/3.0*dr2_dxi - poly_part*kSqrt5*dr_dxi);
  }
}

void MaternNu2p5::HyperparameterGradCovariance(double const * restrict point_one, double const * restrict point_two,
                                               double * restrict grad_hyperparameter_cov) const noexcept {
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  if (norm_val == 0.0) {
    grad_hyperparameter_cov[0] = 1.0;
    std::fill(grad_hyperparameter_cov+1, grad_hyperparameter_cov + 1+dim_, 0.0);
    return;
  }
  const double matern_arg = kSqrt5 * std::sqrt(norm_val);
  const double poly_part = matern_arg + 5.0/3.0*norm_val;
  const double exp_part = std::exp(-matern_arg);

  // deriv wrt alpha does not have the same form as the length terms, special case it
  grad_hyperparameter_cov[0] = (1.0 + poly_part) * exp_part;
  // terms from differentiating Covariance wrt spatial dimensions; since exp(x) is the derivative's identity, some cancellation of
  // analytic 0s is possible (and desired since it reduces compute-time and is more accurate)
  for (int i = 0; i < dim_; ++i) {
    const double dr2_dleni = -2.0*Square((point_one[i] - point_two[i])/lengths_[i])/lengths_[i];
    const double dr_dleni = 0.5*dr2_dleni/std::sqrt(norm_val);
    grad_hyperparameter_cov[i+1] = alpha_*exp_part*(5.0/3.0*dr2_dleni - poly_part*kSqrt5*dr_dleni);
  }
}

void MaternNu2p5::HyperparameterHessianCovariance(double const * restrict point_one, double const * restrict point_two,
                                                  double * restrict hessian_hyperparameter_cov) const noexcept {
  const int num_hyperparameters = GetNumberOfHyperparameters();
  const double norm_val = NormSquaredWithInverseWeights(point_one, point_two, lengths_sq_.data(), dim_);
  if (norm_val == 0.0) {
    std::fill(hessian_hyperparameter_cov, hessian_hyperparameter_cov + Square(num_hyperparameters), 0.0);
    return;
  }

  const double sqrt_norm_val = std::sqrt(norm_val);
  const double matern_arg = kSqrt5 * sqrt_norm_val;
  const double poly_part = matern_arg + 5.0/3.0*norm_val;
  const double exp_part = std::exp(-matern_arg);

  // precompute deriv of r wrt length hyperparameters, where r is NormSquaredWithInverseWeights
  std::vector<double> dr_dlen(num_hyperparameters);
  for (int i = 0; i < dim_; ++i) {
    // note the indexing is shifted by one here so that we can iterate over componentwise_scaled_distance with the
    // same index that is used for hyperparameter indexing
    dr_dlen[i+1] = -Square((point_one[i] - point_two[i])/lengths_[i])/lengths_[i]/sqrt_norm_val;  // -1/r*(x1_i - x2_i)^2/l_i^3
  }

  double const * restrict hessian_hyperparameter_cov_row = hessian_hyperparameter_cov + 1;  // used to index through rows
  // first column of hessian, derivatives of pderiv{cov}{\theta_0} with respect to \theta_i
  // \theta_0 is alpha, the scaling factor; its derivatives are fundamentally different than those wrt length scales; hence
  // they are split out of the loop
  // deriv wrt alpha does not have the same form as the length terms, special case it
  hessian_hyperparameter_cov[0] = 0.0;
  for (int i = 1; i < num_hyperparameters; ++i) {
    // this is simply pderiv{cov}{\theta_i}/alpha, i = 1..dim+1
    const double dr2_dlenj = 2.0*sqrt_norm_val*dr_dlen[i];
    hessian_hyperparameter_cov[i] = exp_part*(5.0/3.0*dr2_dlenj - poly_part*kSqrt5*dr_dlen[i]);
  }
  hessian_hyperparameter_cov += num_hyperparameters;

  // remaining columns of the hessian: derivatives with respect to pderiv{cov}{\theta_j} for j = 1..dim+1 (length scales)
  // terms from differentiating HyperparameterGradCovariance wrt spatial dimensions; since exp(x) is the derivative's identity,
  // some cancellation of analytic 0s is possible (and desired since it reduces compute-time and is more accurate)
  for (int j = 1; j < num_hyperparameters; ++j) {
    // copy j-th column from j-th row, which has already been computed
    for (int i = 0; i < j; ++i) {
      hessian_hyperparameter_cov[i] = hessian_hyperparameter_cov_row[0];
      hessian_hyperparameter_cov_row += num_hyperparameters;
    }
    hessian_hyperparameter_cov_row -= j*num_hyperparameters;  // reset row for next iteration

    // on diagonal component has extra terms since normally dx_i/dx_j = 0 except for i == j
    // the RHS terms are only read from already-computed or copied components
    const double dr2_dlenj = 2.0*sqrt_norm_val*dr_dlen[j];
    hessian_hyperparameter_cov[j] = -alpha_*kSqrt5*dr_dlen[j]*(hessian_hyperparameter_cov[0] +
                                                               exp_part*(5.0/3.0*sqrt_norm_val*dr_dlen[j] -
                                                                         (matern_arg + 5.0/3.0*norm_val)*3.0/lengths_[j-1])) +
        alpha_*exp_part*(-5.0*dr2_dlenj/lengths_[j-1]);
    // remaining off-digaonal terms
    for (int i = j+1; i < num_hyperparameters; ++i) {
      hessian_hyperparameter_cov[i] = -alpha_*kSqrt5*dr_dlen[i]*(hessian_hyperparameter_cov[0] +
                                                                 exp_part*5.0/3.0*sqrt_norm_val*dr_dlen[j]);
    }

    hessian_hyperparameter_cov += num_hyperparameters;
    hessian_hyperparameter_cov_row += 1;
  }
}

CovarianceInterface * MaternNu2p5::Clone() const {
  return new MaternNu2p5(*this);
}

}  // end namespace optimal_learning
